const assert = require('assert');
const ganache = require('ganache-cli');
const Web3 = require('web3');
const { interface, bytecode } = require('../compile');

const provider = ganache.provider();
const web3 = new Web3(provider);


let accounts;
let inbox;
const INITIAL_STRING = 'HI there!';

beforeEach(async () => {
    //get accounts
    accounts = await web3.eth.getAccounts();
    //deploy contract | in inbox we have a reference to the inbox contract (object)
    inbox = await new web3.eth.Contract(JSON.parse(interface))
    .deploy({ data: bytecode, arguments: [INITIAL_STRING] })
    .send({ from: accounts[0], gas: '1000000' });
    
    inbox.setProvider(provider);
});

describe('We will test the Inbox Contract', function() {

    it('deploys a contract', function() {
        assert.ok(inbox.options.address);
    })

    it('has a default message', async function() {
        let message = await inbox.methods.message().call();
        assert.equal(message, INITIAL_STRING);
    });

    it('can change a message', async function() {
        await inbox.methods.setMessage('BYE there').send({ from: accounts[0] });
        let newMessage = await inbox.methods.message().call();
        assert.equal(newMessage, 'BYE there');
    })

});